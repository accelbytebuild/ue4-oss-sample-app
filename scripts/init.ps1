# Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager

if ( $Env:BB_BRANCH -eq "" ) { exit 1 }

pushd
pwd

echo "CI_BUILDS_DIR: "
echo $Env:CI_BUILDS_DIR

function CloneRepository() {
    git clone --recurse-submodules --remote-submodules git@bitbucket.org:accelbyte/$Env:REPO_NAME.git
    cd $Env:REPO_NAME
    git checkout $Env:BB_BRANCH
    git submodule update --recursive --remote
    echo "Checkout Done"   
}

function UpdateExistingDirectory() {
    cd $Env:REPO_NAME

    git clean -xfd
    git submodule foreach --recursive git clean -xfd
    git reset --hard
    git submodule foreach --recursive git reset --hard
    git submodule update --recursive --remote

    git checkout $Env:BB_BRANCH
    git pull

    git clean -xfd
    git submodule foreach --recursive git clean -xfd
    git reset --hard origin/$Env:BB_BRANCH
    git submodule foreach --recursive git reset --hard
    git submodule update --recursive --remote

    echo "Update Repository Done"
}

if (!(Test-Path $Env:CI_BUILDS_DIR)) {
    mkdir $Env:CI_BUILDS_DIR
}

cd $Env:CI_BUILDS_DIR

if (Test-Path $Env:CI_BUILDS_DIR\$Env:REPO_NAME) {
    UpdateExistingDirectory
    echo "Current dir after update:"
    pwd
}
else {
    CloneRepository
    echo "Current dir after clone:"
    pwd
}
