# Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager

$JUSTICE_UPLOADER = "C:\GitLab-Runner\accelbyte_uploader_tool\justice-build-util-3.14.0-alpha.1-windows_amd64.exe"
$S3_UPLOADER = "C:\GitLab-Runner\accelbyte_uploader_tool\windows-amd64.exe"
$SDK_CONFIG_LOCATION = "C:\GitLab-Runner\accelbyte_uploader_tool\AccelByteSDKConfigABS.json"

$PROJECT_REPO_PATH="$Env:CI_BUILDS_DIR\$Env:REPO_NAME"

if (Test-Path $PROJECT_REPO_PATH\"version.json") 
{
    $VERSIONS_JSON = cat $PROJECT_REPO_PATH\"version.json"
    $VERSIONS_JSON = $VERSIONS_JSON | ConvertFrom-Json

    .\scripts\batch\Upload.bat $PROJECT_REPO_PATH $JUSTICE_UPLOADER $S3_UPLOADER $VERSIONS_JSON.Server $VERSIONS_JSON.Client $SDK_CONFIG_LOCATION
    if ($LastExitCode -gt 0)
    {
        .\scripts\slack_notifier.ps1 upload_failed $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client  $VERSIONS_JSON.Server
        Write-Error "Upload failed"
        exit(1)
    }
}
else
{
    .\scripts\slack_notifier.ps1 upload_skipped $Env:REPO_NAME $PROJECT_REPO_PATH "" ""
    Write-Warning("Build success but skipped upload, " + $PROJECT_REPO_PATH + "\version.json doesn't exist")
    exit(0)
}

.\scripts\slack_notifier.ps1 upload_done $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client $VERSIONS_JSON.Server
