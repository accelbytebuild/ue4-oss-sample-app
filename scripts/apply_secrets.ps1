# Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager

$CONFIG_LOCATION="$Env:CI_BUILDS_DIR\$Env:REPO_NAME\Config\"
$NAMESPACE = $Env:GAME_CLIENT_NAMESPACE
$CLIENT_CLIENT_ID = $Env:GAME_CLIENT_CLIENT_ID
$CLIENT_SECRET = $Env:GAME_CLIENT_SECRET
$SERVER_CLIENT_ID = $Env:GAME_SERVER_CLIENT_ID
$SERVER_SECRET = $Env:GAME_SERVER_SECRET

if($NAMESPACE -eq "")
{
	echo "cannot set the secret, GAME_NAMESPACE in CI/CD variable is not set"
	exit(1)
}

if($CLIENT_CLIENT_ID -eq "")
{
	echo "cannot set the secret, GAME_CLIENT_CLIENT_ID in CI/CD variable is not set"
	exit(1)
}

if($CLIENT_SECRET -eq "")
{
	echo "GAME_CLIENT_SECRET in CI/CD variable is not set"
}

if($SERVER_CLIENT_ID -eq "")
{
	echo "cannot set the secret, GAME_SERVER_CLIENT_ID in CI/CD variable is not set"
	exit(1)
}

if($SERVER_SECRET -eq "")
{
	echo "GAME_SERVER_SECRET in CI/CD variable is not set"
}

if($CONFIG_LOCATION -eq "")
{
	echo "cannot set the secret, SDK_CONFIG_LOCATION is not set"
	exit(1)
}

if (!(Test-Path $CONFIG_LOCATION)) 
{
    echo "cannot set the secret, SDK_CONFIG_LOCATION points to wrong location. value: $CONFIG_LOCATION"
	exit(1)
}

pushd
echo "applying the secret sauce..."
cd $CONFIG_LOCATION 
ls

$isAnythingWasSet = $false
if (Test-Path "DefaultEngine.ini")
{
    echo "found DefaultEngine.ini"
	$iniData = (cat DefaultEngine.ini)
	$iniData = $iniData.replace("Namespace=CONTACT-US", "Namespace=$NAMESPACE")
	$iniData = $iniData.replace("ClientId=CONTACT-US", "ClientId=$SERVER_CLIENT_ID")
	$iniData = $iniData.replace("ClientSecret=CONTACT-US", "ClientSecret=$SERVER_SECRET")
	echo $iniData > DefaultEngine.ini
	echo "replace done"
	$isAnythingWasSet = $true
}

popd

$PROJECT_REPO_PATH = "$Env:CI_BUILDS_DIR\$Env:REPO_NAME"
$VERSIONS_JSON = ""
if (Test-Path ($PROJECT_REPO_PATH.ToString() + "\version.json")) 
{
	$VERSIONS_JSON = Get-content -Path ($PROJECT_REPO_PATH.ToString() +"\version.json")
	$VERSIONS_JSON = $VERSIONS_JSON | ConvertFrom-Json
}

if($isAnythingWasSet -eq $true) 
{
	if ($VERSIONS_JSON -ne "") 
	{
		.\scripts\slack_notifier.ps1 build_start $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client $VERSIONS_JSON.Server
	}
	else
	{
		.\scripts\slack_notifier.ps1 build_start $Env:REPO_NAME $PROJECT_REPO_PATH "" ""
	}
	echo "Anything was set"
} 
else
{
	if ($VERSIONS_JSON -ne "")
	{
		.\scripts\slack_notifier.ps1 build_failed $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client $VERSIONS_JSON.Server
	}
	else
	{
		.\scripts\slack_notifier.ps1 build_failed $Env:REPO_NAME $PROJECT_REPO_PATH "" ""
	}
	echo "Anything was not set"
	exit(1)
}
