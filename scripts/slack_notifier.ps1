# Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager

function Get-SlackId($channelName)
{
    $token = $SLACK_TOKEN
    $headers = @{
        Authorization="Bearer $token"
    }
    $Form = @{
        exclude_archived = "true"
        limit = 1500
        types = "public_channel,private_channel"
     }
    $Response = Invoke-RestMethod -Uri "https://slack.com/api/conversations.list" -Method POST  -ContentType "application/x-www-form-urlencoded" -Headers $headers -Body $Form
    echo $Response
    if ($Response.ok -eq $true) 
    {
        $Channels = $Response.channels
        foreach ($channel in $Channels) 
        {
           if($channel.name -eq $channelName)
           {
               return $channel.id.ToString()
           }
        }
    }
    echo ("Unable to find channel " +  $channelName)
    popd
    exit(0)
}

#config here
$SLACK_CHANNEL_NAME = $Env:SLACK_CHANNEL_NAME
$SLACK_CHANNEL_ID = $Env:SLACK_CHANNEL_ID
$STATUS = $args[0]
$REPO_NAME = $args[1]
$PROJECT_REPO_PATH = $args[2]
$CLIENT_VERSION = $args[3]
$SERVER_VERSION = $args[4]
$SLACK_TOKEN = $Env:AUTOMATION_SLACK_TOKEN
$SLACK_POST_URL = $Env:SLACK_POST_URL
$CI_PIPELINE_URL = $Env:CI_PIPELINE_URL
$USERNAME = $Env:NAME
#end

if ($USERNAME -eq "none" -or $USERNAME -eq "" -or $USERNAME -eq "YOUR-NAME")
{
    $USERNAME = "No Name"
}

pushd

echo "posting the result to slack..."

if($SLACK_TOKEN -eq "")
{
    echo "AUTOMATION_SLACK_TOKEN is empty!"
    echo "Will not report to slack"
    popd
    exit(0)
}

if($SLACK_POST_URL -eq "")
{
    echo "SLACK_POST_URL is empty!"
    echo "Will not report to slack"
    popd
    exit(0)
}

Get-SlackId($SLACK_CHANNEL_NAME)

if($PROJECT_REPO_PATH -eq "" -Or !(Test-Path $PROJECT_REPO_PATH))
{
    echo "PROJECT_REPO_PATH is empty! or"
    echo "$PROJECT_REPO_PATH is not found"
    echo "Will not report to slack"
    popd
    exit(0)
}

cd $PROJECT_REPO_PATH

if($SERVER_VERSION -eq "")
{
    $SERVER_VERSION = "Not specified in the manifest"
}
if($CLIENT_VERSION -eq "")
{
    $CLIENT_VERSION = "Not specified in the manifest"
}

echo "commited by $USERNAME"

$SLACK_ATTACHMENTS_COLOR = "#E01E5A"
$PIPELINE_STATUS = ""
$HEADER = "BUILD NOTIFICATION"
if ($args[0] -eq "build_start")
{
    $SLACK_ATTACHMENTS_COLOR = "#00c8ff"
    $PIPELINE_STATUS = "STARTING TO BUILD"
    $HEADER = "START BUILD NOTIFICATION"
}
elseif ($args[0] -eq "build_done") 
{    
    $SLACK_ATTACHMENTS_COLOR = "#2EB67D"
    $PIPELINE_STATUS = "BUILD SUCCESSFUL"
}
elseif($args[0] -eq "build_failed")
{
    $PIPELINE_STATUS = "BUILD FAILED"
}
elseif($args[0] -eq "upload_done")
{
    $SLACK_ATTACHMENTS_COLOR = "#2EB67D"
    $PIPELINE_STATUS = "UPLOAD SUCCESSFUL"
    $HEADER = "UPLOAD NOTIFICATION"
}
elseif($args[0] -eq "upload_failed")
{
    $PIPELINE_STATUS = "UPLOAD FAILED"
    $HEADER = "UPLOAD NOTIFICATION"
}
elseif($args[0] -eq "upload_skipped")
{
    $SLACK_ATTACHMENTS_COLOR = "#f0bf11"
    $PIPELINE_STATUS = "UPLOAD SKIPPED"
    $HEADER = "UPLOAD NOTIFICATION"
}
else 
{
    $PIPELINE_STATUS = "UNKNOWN"
}

$SLACK_MESSAGE = @"
{
    "channel" : "$SLACK_CHANNEL_ID",
    "attachments" : [
        {
            "color" : "$SLACK_ATTACHMENTS_COLOR",
            "blocks" : [
                {
                    "type" : "header",
                    "text" : {
                         "type" : "plain_text",
                         "text" : "$HEADER",
                         "emoji" : true
                    }
                },            
                {
                    "type" : "section",
                    "fields" : [
                        {
                            "type" : "mrkdwn",
                            "text" : "*Service:*\n$REPO_NAME"
                        },
                        {
                            "type" : "mrkdwn",
                            "text" : "*Environment:*\n Demo"
                        },
                        {
                            "type" : "mrkdwn",
                            "text" : "* Client Version:*\n$CLIENT_VERSION"
                        },                    
                        {
                            "type" : "mrkdwn",
                            "text" : "*Commit By:*\n $USERNAME"
                        },           
                        {
                            "type" : "mrkdwn",
                            "text" : "* Server Version:*\n$SERVER_VERSION"
                        },
                        {
                            "type" : "mrkdwn",
                            "text" : "*Status:*\n$PIPELINE_STATUS"
                        }
                    ]
                },
                {
                    "type" : "actions",
                    "block_id" : "actionblock789",
                    "elements" : [
                        {
                            "type" : "button",
                            "text" : {
                                "type" : "plain_text",
                                "text" : ":gitlab: View Pipeline"
                            },
                            "url" : "$CI_PIPELINE_URL"
                        }
                    ]
                }
            ]
        }
    ]
}
"@

$Response = Invoke-RestMethod -Uri $SLACK_POST_URL -Method POST -ContentType "application/json" -Body $SLACK_MESSAGE

popd
