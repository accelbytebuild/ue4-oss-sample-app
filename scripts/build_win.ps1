# Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager

$PROJECT_REPO_PATH="$Env:CI_BUILDS_DIR\$Env:REPO_NAME"

popd
pwd

$VERSIONS_JSON = ""
if (Test-Path ($PROJECT_REPO_PATH.ToString() + "\version.json")) 
{
    $VERSIONS_JSON = Get-content -Path ($PROJECT_REPO_PATH.ToString() +"\version.json")
    $VERSIONS_JSON = $VERSIONS_JSON | ConvertFrom-Json
}

# build process
.\scripts\batch\Cleanup.bat $PROJECT_REPO_PATH
.\scripts\batch\GenerateProjectSolution.bat $PROJECT_REPO_PATH $Env:USE_PACKED
#end

if ($LastExitCode -gt 0)
{
    if ($VERSIONS_JSON -eq "")
    {
        .\scripts\slack_notifier.ps1 build_failed $Env:REPO_NAME $PROJECT_REPO_PATH "" ""
        Write-Error "Build Failed"
        exit(1)
    }
    else
    {
        .\scripts\slack_notifier.ps1 build_failed $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client $VERSIONS_JSON.Server
        Write-Error "Build Failed"
        exit(1)
    }
}

if ($VERSIONS_JSON -eq "")
{
    .\scripts\slack_notifier.ps1 build_done $Env:REPO_NAME $PROJECT_REPO_PATH "" ""
    Write-Error "Build Done"
    exit(0)
}
else
{
    .\scripts\slack_notifier.ps1 build_done $Env:REPO_NAME $PROJECT_REPO_PATH $VERSIONS_JSON.Client $VERSIONS_JSON.Server
    Write-Error "Build Done"
    exit(0)
}
