REM Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
REM This is licensed software from AccelByte Inc, for limitations
REM and restrictions contact your company contract manager

::Re-generate project solution
@echo off
Setlocal EnableDelayedExpansion
set PROJECT_REPO_PATH=%~1
set USE_PACKED=%~2

if defined USE_PACKED (
	set USE_PACKED=-package
	echo Packed configuration!
) 

cd %PROJECT_REPO_PATH%
SET PROJECT_PATH=%PROJECT_REPO_PATH%\OSSDemo.uproject
SET CLIENT_OUTPUT=%PROJECT_REPO_PATH%\Output\
SET SERVER_OUTPUT=%PROJECT_REPO_PATH%\Output_Server\

echo ======================Building windows client==================

call %ENGINE_UAT_PATH% -ScriptsForProject=%PROJECT_PATH% BuildCookRun -nop4 -platform=Win64 -clientconfig=DebugGame -project=%PROJECT_PATH% -cook -stage -archive -archivedirectory=%CLIENT_OUTPUT% -prereqs -targetplatform=Win64 -build -ShooterGame -utf8output %USE_PACKED%
echo Windows build returns %ERRORLEVEL%
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%

echo ======================Building linux server====================

call %ENGINE_UAT_PATH% -ScriptsForProject=%PROJECT_PATH% BuildCookRun -nop4 -platform=Linux -clientconfig=DebugGame -project=%PROJECT_PATH% -cook -stage -archive -archivedirectory=%SERVER_OUTPUT% -prereqs -targetplatform=Linux -build -ShooterGame -utf8output -server -serverplatform=Linux -noclient %USE_PACKED%
echo Linux build returns %ERRORLEVEL%
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%
