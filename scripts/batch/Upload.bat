REM Copyright (c) 2021 AccelByte Inc. All Rights Reserved.
REM This is licensed software from AccelByte Inc, for limitations
REM and restrictions contact your company contract manager

@echo off
Setlocal EnableDelayedExpansion

set string=abcdefghijklmnopqrstuvwxyz
set result=
for /L %%i in (1,1,30) do call :add
echo BUILD_%result%
goto :main

:add
set /a x=%random% %% 26 
set result=%result%!string:~%x%,1!
goto :eof

:main

set PROJECT_PATH=%~1
set JUSTICE_EXE=%~2
set S3_EXE=%~3
set SERVER_VERSION=%~4
set CLIENT_VERSION=%~5
set SDK_CONFIG_PATH=%~6
SET COPYCMD=/Y

cd PROJECT_PATH

if not defined SERVER_VERSION (
	echo SERVER_VERSION not defined. exiting
	exit 1
)
if not defined CLIENT_VERSION (
	echo CLIENT_VERSION not defined. exiting
	exit 1
)

::move debug symbols

if not exist %PROJECT_PATH%\AbshooterDebugSymbol\client ( mkdir  %PROJECT_PATH%\AbshooterDebugSymbol\client )
if not exist %PROJECT_PATH%\AbshooterDebugSymbol\server ( mkdir  %PROJECT_PATH%\AbshooterDebugSymbol\server )
move %PROJECT_PATH%\Output\WindowsNoEditor\ShooterGame\Binaries\Win64\ShooterGame-Win64-DebugGame.pdb %PROJECT_PATH%\AbshooterDebugSymbol\client\ShooterGame-Win64-DebugGame.pdb
move %PROJECT_PATH%\Output_Server\LinuxServer\ShooterGame\Binaries\Linux\ShooterGameServer.debug %PROJECT_PATH%\AbshooterDebugSymbol\server\ShooterGameServer.debug
move %PROJECT_PATH%\Output_Server\LinuxServer\ShooterGame\Binaries\Linux\ShooterGameServer.sym %PROJECT_PATH%\AbshooterDebugSymbol\server\ShooterGameServer.sym

::Upload server to S3

SET HOSTNAME=https://demo.accelbyte.io
SET NAMESPACE=abshooter
SET CLIENT_ID=8688fce6c3a7448d8614e15bf7771149
SET VERSION=%SERVER_VERSION%
SET DS_STARTUP_COMMANDFILE=ShooterGameServer.sh
SET SERVER_PATH=%PROJECT_PATH%\Output_Server\LinuxServer
SET S3_BUCKETNAME=justice-ds-upload-service-demo
SET S3_DIRECTORY=BUILD_%result%

call %S3_EXE% syncFolder --hostname=%HOSTNAME% --namespace=%NAMESPACE% --id=%CLIENT_ID% --version=%VERSION% --command=%DS_STARTUP_COMMANDFILE% -p %SERVER_PATH% -b %S3_BUCKETNAME% -d %S3_DIRECTORY%
echo S3 uploads returns %ERRORLEVEL%
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%

::Upload client to Justice

SET BIN_PATH=%PROJECT_PATH%\Output\WindowsNoEditor
SET SDKCONFIG_PATH=%SDK_CONFIG_PATH%
SET PUBLISHER=accelbyte
SET APP_ID=abshooterid123123
SET VERSION=%CLIENT_VERSION%
SET EXE=ShooterGame.exe

echo Y | call %JUSTICE_EXE% upload -n %PUBLISHER% -a %APP_ID% -v %VERSION% -e %EXE% -f %SDKCONFIG_PATH% -d %BIN_PATH% -p win64 -u https://demo.accelbyte.io
echo JUSTICE uploads returns %ERRORLEVEL%
IF %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%
